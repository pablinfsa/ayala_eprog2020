/*==================[inlcusiones]============================================*/
#include "lcd.h"
#include "gpio.h"
#include "delay.h"
/*==================[definiciones y macros]==================================*/

/*==================[definiciones de datos internos]=========================*/

typedef struct {
   uint16_t lineWidth;
   uint16_t amountOfLines;
   uint16_t charWidth;
   uint16_t charHeight;
   uint8_t x;
   uint8_t y;
} lcd_t;

/*==================[definiciones de datos externos]=========================*/

static lcd_t lcd;

/*==================[declaraciones de funciones internas]====================*/

//static uint8_t lcdDataValue = 0;
//static uint8_t lcdEnStatus = OFF;
//static uint8_t lcdRsStatus = OFF;
//static uint8_t lcdBacklightStatus = 0;




static void lcdEnablePulse( void );
static void lcdSendNibble( uint8_t nibble );

/*==================[declaraciones de funciones externas]====================*/

/*==================[definiciones de funciones internas]=====================*/

void lcdPinSet( uint8_t pin, bool state )//setea un pin en alto o bajo
{
	GPIOState( pin, state);
}

static void lcdEnablePulse( void )
{
	lcdPinSet( LCD_HD44780_EN, ON );       // EN = 1 for H-to-L pulse
	DelayUs( LCD_EN_PULSE_WAIT_US );       // Wait to make EN wider
	lcdPinSet( LCD_HD44780_EN, OFF );      // EN = 0 for H-to-L pulse
    DelayUs(100); // commands need > 37us to settle
}

static void lcdSendNibble( uint8_t nibble )
{
	lcdPinSet( LCD_HD44780_D7, ( nibble & 0x80 ) );//setea el bit menos significativo en alto o bajo
	lcdPinSet( LCD_HD44780_D6, ( nibble & 0x40 ) );
	lcdPinSet( LCD_HD44780_D5, ( nibble & 0x20 ) );
	lcdPinSet( LCD_HD44780_D4, ( nibble & 0x10 ) );
}

/*==================[definiciones de funciones externas]=====================*/

void lcdCommand( uint8_t cmd )
{
   lcdSendNibble( cmd & 0xF0 );          // Send high nibble to D7-D4

   lcdPinSet( LCD_HD44780_RS, OFF );   // RS = 0 for command
   //lcdPinSet( LCD_HD44780_RW, OFF );   // RW = 0 for write

   lcdEnablePulse();
   DelayUs( LCD_LOW_WAIT_US );       // Wait

   lcdSendNibble( cmd << 4 );            // Send low nibble to D7-D4
   lcdEnablePulse();
}

void lcdData( uint8_t data )
{
   lcdSendNibble( data & 0xF0 );         // Send high nibble to D7-D4

   lcdPinSet( LCD_HD44780_RS, ON );    // RS = 1 for data
   //lcdPinSet( LCD_HD44780_RW, OFF );   // RW = 0 for write

   lcdEnablePulse();
   lcdSendNibble( data << 4 );           // Send low nibble to D7-D4
   lcdEnablePulse();
}

void lcdInit( uint16_t lineWidth, uint16_t amountOfLines,
              uint16_t charWidth, uint16_t charHeight )
{
   lcd.lineWidth = lineWidth;
   lcd.amountOfLines = amountOfLines;
   lcd.charWidth = charWidth;
   lcd.charHeight = charHeight;
   lcd.x = 0;
   lcd.y = 0;

   // Configure LCD Pins as Outputs
   GPIOInit( LCD_HD44780_RS, GPIO_OUTPUT);
   //GPIOInit( LCD_HD44780_RW, GPIO_OUTPUT);
   GPIOInit( LCD_HD44780_EN, GPIO_OUTPUT);
   GPIOInit( LCD_HD44780_D4, GPIO_OUTPUT);
   GPIOInit( LCD_HD44780_D5, GPIO_OUTPUT);
   GPIOInit( LCD_HD44780_D6, GPIO_OUTPUT);
   GPIOInit( LCD_HD44780_D7, GPIO_OUTPUT);

   // Configure LCD for 4-bit mode
   //lcdPinSet( LCD_HD44780_RW, OFF );     // RW = 0
   lcdPinSet( LCD_HD44780_RS, OFF );     // RS = 0
   lcdPinSet( LCD_HD44780_EN, OFF );     // EN = 0

   lcdCommand( 0x33 );                   // Command 0x33 for 4-bit mode
   lcdCommandDelay();                    // Wait

   lcdCommand( 0x32 );                   // Command 0x32 for 4-bit mode
   lcdCommandDelay();                    // Wait

   lcdCommand( 0x28 );                   // Command 0x28 for 4-bit mode
   lcdCommandDelay();                    // Wait

   // Initialize LCD
   lcdCommand( 0x0E );                   // Command 0x0E for display on, cursor on
   lcdCommandDelay();                    // Wait

   lcdClear();                           // Command for clear LCD

   lcdCommand( 0x06 );                   // Command 0x06 for Shift cursor right
   lcdCommandDelay();                    // Wait

   DelayMs(1);                     // Wait

   lcdCursorSet( LCD_CURSOR_OFF );
   //lcdGoToXY( 0, 0 );
   lcdClearAndHome();
}

void lcdGoToXY( uint8_t x, uint8_t y )
{
   if( x >= lcd.lineWidth || y >= lcd.amountOfLines ) {
      return;
   }
   uint8_t firstCharAdress[] = { 0x80, 0xC0, 0x94, 0xD4 };   // See table 12-5
   //lcdCommand( firstCharAdress[ y - 1 ] + x - 1 ); // Start in {x,y} = {1,1}
   lcdCommand( firstCharAdress[y] + x );             // Start in {x,y} = {0,0}
   DelayUs( LCD_HIGH_WAIT_US );      // Wait
   lcd.x = x;
   lcd.y = y;
}

void lcdClear( void )
{
   lcdCommand( 0x01 );                   // Command 0x01 for clear LCD
   DelayMs(LCD_CLR_DISP_WAIT_MS);    // Wait
}

void lcdCursorSet( lcdCursorModes_t mode )
{
   lcdCommand( 0b00001100 | mode );
   DelayMs(LCD_CLR_DISP_WAIT_MS); // Wait
}

void lcdSendStringRaw( char* str )
{
   uint8_t i = 0;
   while( str[i] != 0 ) {
      lcdData( str[i] );
      i++;
   }
}

void lcdCreateChar( uint8_t charnum, const char* chardata )
{
   uint8_t i;
   charnum &= 0x07;
   lcdCommand( E_SET_CGRAM_ADDR | (charnum << 3) );
   for (i = 0; i < 8; i++) {
      lcdData( chardata[i] );
   }
   DelaySec(1);
   lcdGoToXY( lcd.x, lcd.y );
}

void lcdCreateCustomChar( lcdCustomChar_t* customChar )
{
   lcdCreateChar( customChar->address, customChar->bitmap );
}

void lcdSendCustomChar( lcdCustomChar_t* customChar )
{
   lcdSendCustomCharByIndex( customChar->address );
}


void lcdClearAndHome( void )
{
   lcdClear();
   lcdGoToXY( 0, 0 ); // Poner cursor en 0, 0
   //delay(100);
}

void lcdClearLine( uint8_t line )
{
   lcdClearLineFromTo( line, 0, lcd.lineWidth - 1 );
}

void lcdClearLineFrom( uint8_t line, uint8_t xFrom )
{
   lcdClearLineFromTo( line, xFrom, lcd.lineWidth - 1 );
}

void lcdClearLineFromTo( uint8_t line, uint8_t xFrom, uint8_t xTo )
{
   uint8_t i = 0;

   if( xFrom >= lcd.lineWidth || line >= lcd.amountOfLines ) {
      return;
   }
   if( xFrom > xTo ) {
      return;
   }
   if( xTo >= lcd.lineWidth ) {
      xTo = lcd.lineWidth - 1;
   }

   lcdGoToXY( xFrom, line );
   for( i=xFrom; i<=xTo; i++ ) {
      lcdSendChar( ' ' );
   }
   //lcd.x--;
   lcdGoToXY( xFrom, line );
}

void lcdSendChar( char character )
{
   //uint8_t i = 0;

   if( character == '\r' ) {        // Ignore '\r'
   } else if( character == '\n' ) { // Mando enter
      lcdSendEnter();
   } else {
      // Si se extiende en ancho mando enter
      if( lcd.x >= lcd.lineWidth ) {
         lcdSendEnter();
      }
      // Mando el caracter
      lcdData( character );
      lcd.x++;
   }
}

void lcdSendCustomCharByIndex( uint8_t charIndex )
{
   // Si se extiende en ancho mando enter
   if( lcd.x >= lcd.lineWidth ) {
      lcdSendEnter();
   }
   // Mando el caracter
   lcdData( charIndex );
   lcd.x++;
}

void lcdSendEnter( void )
{
   // Si llego abajo no hace nada
   if( lcd.y >= lcd.amountOfLines ) {
      return;
   } else {
      lcd.x = 0;
      lcd.y++;
      lcdGoToXY( lcd.x, lcd.y );
   }
}

void lcdSendStringClearLine( char* str )
{
   lcdSendString( str );
   lcdClearLineFrom( lcd.y, lcd.x );
}

void lcdSendString( char* str )
{
   uint32_t i = 0;
   while( str[i] != 0 ) {
      lcdSendChar( str[i] );
      i++;
   }
}

void lcdSendStringFormXY( char* str, uint8_t x, uint8_t y )
{
   lcdGoToXY( x, y );
   lcdSendString( str );
}

void lcdSendStringFormXYClearLine( char* str, uint8_t x, uint8_t y )
{
   lcdSendStringFormXY( str, x, y );
   lcdClearLineFrom( lcd.y, lcd.x );
}

void lcdSendInt( int64_t value )
{
   lcdSendString( intToStringGlobal(value) );
}

void lcdSendIntClearLine( int64_t value )
{
   lcdSendInt( value );
   lcdClearLineFrom( lcd.y, lcd.x );
}

void lcdSendIntFormXY( int64_t value, uint8_t x, uint8_t y )
{
   lcdGoToXY( x, y );
   lcdSendInt( value );
}

void lcdSendIntFormXYClearLine( int64_t value, uint8_t x, uint8_t y )
{
   lcdSendIntFormXY( value, x, y );
   lcdClearLineFrom( lcd.y, lcd.x );
}


void lcdSendFloat( float value, uint32_t decDigits )
{
   lcdSendString( floatToStringGlobal(value, decDigits) );
}

void lcdSendFloatClearLine( float value, uint32_t decDigits )
{
   lcdSendString( floatToStringGlobal(value, decDigits) );
   lcdClearLineFrom( lcd.y, lcd.x );
}

void lcdSendFloatFormXY( float value, uint32_t decDigits, uint8_t x, uint8_t y )
{
   lcdGoToXY( x, y );
   lcdSendFloat( value, decDigits );
}

void lcdSendFloatFormXYClearLine( float value, uint32_t decDigits, uint8_t x, uint8_t y )
{
   lcdSendFloatFormXY( value, decDigits, x, y );
   lcdClearLineFrom( lcd.y, lcd.x );
}

/*==================[fin del archivo]========================================*/
