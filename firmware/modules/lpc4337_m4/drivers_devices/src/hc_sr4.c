
/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "bool.h"
#include "delay.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/

uint16_t N_cm=40;
uint16_t N_in=129;
/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/
/*==================[internal data definition]===============================*/
/*==================[external data definition]===============================*/
/*==================[internal functions definition]==========================*/
/*==================[external functions definition]==========================*/


	bool HcSr04Init(gpio_t echo, gpio_t trigger)
	{
		GPIOInit(echo, GPIO_INPUT);/*T_FIL2 es ECHO "salida del sensor entrada de la placa"*/
		GPIOInit(trigger, GPIO_OUTPUT);/*T_FIL3 es TRIGGER "entrada del sensor salida de la placa"*/
		return true;
	}

	int16_t HcSr04ReadData(gpio_t echo, gpio_t trigger)
	{
		int16_t cont=0;
		GPIOOn(trigger);/*saco una cuadrada de 10us para inciar el tren de pulsos*/
		DelayUs(10);
		GPIOOff(trigger);

		while(GPIORead(echo)==0)
		{
				/*no hace nada mientras este en bajo ECHO*/
		}

		while(GPIORead(echo))/*cuando detecta un puslo mide su ancho*/
		{
			cont++;
			DelayUs(1);
		}

		return cont;

	}

	void HcSr04Calib(gpio_t echo, gpio_t trigger)
	{
		int16_t i=1;
		int16_t j=1;

		while(i)
		{
			j=(HcSr04ReadData(echo,trigger)/(N_cm));
			if(j<20){N_cm-=1;}
			else {N_cm+=1;}
			if(j==20)
			{
				i=0;
				N_cm-=1;
			}
		}
	}

	/*devuelve la distancia en centimetros*/
	int16_t HcSr04ReadDistanceCentimeters(gpio_t echo, gpio_t trigger)
	{
		int16_t distancia=0;
		distancia=HcSr04ReadData(echo,trigger)/(N_cm);
		return distancia;
	}

	/*devuelve la distancia en pulgadas*/
	int16_t HcSr04ReadDistanceInches(gpio_t echo, gpio_t trigger)
	{
		int16_t distancia=0;
		distancia=HcSr04ReadData(echo, trigger)/(N_in);
		return distancia;
	}

	bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
	{
		/*nada*/
		return true;
	}



/*==================[end of file]============================================*/
