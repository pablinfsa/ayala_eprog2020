
#ifndef HC_SR4_H
#define HC_SR4_H
/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "delay.h"
#include "chip.h"
#include "gpio.h"
/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
	bool Tcrt5000Init(gpio_t dout);
	bool Tcrt5000State(gpio_t dout);
	bool Tcrt5000Deinit(gpio_t dout);

/*==================[end of file]============================================*/
#endif /* HC_SR4_H */
