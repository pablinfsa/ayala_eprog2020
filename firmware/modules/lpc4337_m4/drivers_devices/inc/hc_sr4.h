
#ifndef HC_SR4_H
#define HC_SR4_H
/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "delay.h"
#include "chip.h"
#include "gpio.h"
/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
	bool HcSr04Init(gpio_t echo, gpio_t trigger);//inicializa los puertos del gpio
	int16_t HcSr04ReadData(gpio_t echo, gpio_t trigger);//lee el ancho del pulso del echo
	int16_t HcSr04ReadDistanceCentimeters(gpio_t echo, gpio_t trigger);//retorna la distancia en cm
	int16_t HcSr04ReadDistanceInches(gpio_t echo, gpio_t trigger);//retorna la distancai en pulgadas
	void HcSr04Calib(gpio_t echo, gpio_t trigger);//calibra el sensor con objeto a 20cm
	bool HcSr04Deinit(gpio_t echo, gpio_t trigger);//desinicializa

/*==================[end of file]============================================*/
#endif /* HC_SR4_H */
