
#ifndef _LCD_H_
#define _LCD_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <bool.h>
#include <string.h>
#include <stdlib.h>

/*==================[c++]====================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/
/* Functional states */
#ifndef ON
#define ON     1
#endif
#ifndef OFF
#define OFF    0
#endif

/* Electrical states */
#ifndef HIGH
#define HIGH   1
#endif
#ifndef LOW
#define LOW    0
#endif

/* Logical states */

#ifndef FALSE
#define FALSE  0
#endif
#ifndef TRUE
#define TRUE   (!FALSE)
#endif

/* LCD library configuration - Begin */

// LCD Delay HAL
#define lcdCommandDelay()           DelayUs(LCD_CMD_WAIT_US)

// Configure LCD pins
#define LCD_HD44780_D7          LCD4   // Data bit 7.
#define LCD_HD44780_D6          LCD3   // Data bit 4.
#define LCD_HD44780_D5          LCD2   // Data bit 5.
#define LCD_HD44780_D4          LCD1   // Data bit 4.
//#define LCD_HD44780_BACKLIGHT   GPIO1  // Backlight.
#define LCD_HD44780_EN          LCD_EN  // Enable bit.
//#define LCD_HD44780_RW          GND    // Read/Write bit. R/W = 0 for write, R/W = 1 for read (LCD_RW pin connected to GND).
#define LCD_HD44780_RS          LCD_RS  // Register select bit. RS = 0 to select command register, RS = 1 to select data register.

// LCD delay Times
#define LCD_EN_PULSE_WAIT_US   25    // 25 us
#define LCD_LOW_WAIT_US        25    // 25 us
#define LCD_HIGH_WAIT_US       100   // 100 us
#define LCD_CMD_WAIT_US        45    // Wait time for every command 45 us, except:
#define LCD_CLR_DISP_WAIT_MS   3     // - Clear Display 1.52 ms
#define LCD_RET_HOME_WAIT_MS   3     // - Return Home  1.52 ms
#define LCD_RBUSY_ADDR_WAIT_US 0     // - Read Busy flag and address 0 us
#define LCD_STARTUP_WAIT_MS   1000   // 1000 ms

/* LCD library configuration - End */

// For backward compatibility
#define lcdConfig lcdInit


// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).

// commands
#define LCD_CLEARDISPLAY        0x01
#define LCD_RETURNHOME          0x02
#define LCD_ENTRYMODESET        0x04
#define LCD_DISPLAYCONTROL      0x08
#define LCD_CURSORSHIFT         0x10
#define LCD_FUNCTIONSET         0x20
#define LCD_SETCGRAMADDR        0x40
#define LCD_SETDDRAMADDR        0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT          0x00
#define LCD_ENTRYLEFT           0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON           0x04
#define LCD_DISPLAYOFF          0x00
//#define LCD_CURSORON            0x02
//#define LCD_CURSOROFF           0x00
//#define LCD_BLINKON             0x01
//#define LCD_BLINKOFF            0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE         0x08
#define LCD_CURSORMOVE          0x00
#define LCD_MOVERIGHT           0x04
#define LCD_MOVELEFT            0x00

// flags for function set
#define LCD_8BITMODE            0x10
#define LCD_4BITMODE            0x00
#define LCD_2LINE               0x08
#define LCD_1LINE               0x00
#define LCD_5x10DOTS            0x04
#define LCD_5x8DOTS             0x00


// Set the LCD address to 0x27 for a 16 chars and 2 line display
//LiquidCrystal_I2C lcd(0x27, 16, 2)


/*==================[typedef]================================================*/

// Enumeration defining the HD44780 commands
enum enLcdCommands {
   E_CLEAR_DISPLAY        = 0x01,
   E_RETURN_HOME          = 0x02,
   E_ENTRY_MODE_SET       = 0x04,
   E_DISPLAY_ON_OFF_CTRL  = 0x08,
   E_CURSOR_DISPLAY_SHIFT = 0x10,
   E_FUNCTION_SET         = 0x20,
   E_SET_CGRAM_ADDR       = 0x40,
   E_SET_DDRAM_ADDR       = 0x80
};

// This enumeration defines the available cursor modes
typedef enum{
   LCD_CURSOR_OFF      = 0x00,
   LCD_CURSOR_ON       = 0x02,
   LCD_CURSOR_ON_BLINK = 0x03
} lcdCursorModes_t;

typedef struct{
   const uint8_t address;   // Custom character address
   const uint8_t bitmap[8]; // Custom character bitmap
} lcdCustomChar_t;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// BIBLIOTECA NO REENTRANTE, CUIDADO CON EL RTOS!!!
void lcdPinSet( uint8_t pin, bool state );
void lcdInit( uint16_t lineWidth, uint16_t amountOfLines,
              uint16_t charWidth, uint16_t charHeight );//inicializa el LCD
void lcdCommand( uint8_t cmd );
void lcdData( uint8_t data );

void lcdGoToXY( uint8_t x, uint8_t y );//situa el cursor en un lugar específico
void lcdClear( void );//borra la pantalla
void lcdCursorSet( lcdCursorModes_t mode );//modo del cursor
void lcdCreateChar( uint8_t charnum, const char* chardata );//crea un caracter a partir de un numero

void lcdCreateCustomChar( lcdCustomChar_t* customChar );//crear caracter custom
void lcdSendCustomChar( lcdCustomChar_t* customChar );//enviar caracter custom

void lcdClearAndHome( void );//borra la pantalla y pone el cursor en <0,0>
void lcdClearLine( uint8_t line );//borra la linea del lcd
void lcdClearLineFrom( uint8_t line, uint8_t xFrom );//borra desde ubicacion especifica
void lcdClearLineFromTo( uint8_t line, uint8_t xFrom, uint8_t xTo );//borra un tramo especifico
void lcdSendStringRaw( char* str );//envia una string en linea completa

void lcdSendEnter( void );
void lcdSendChar( char character );
void lcdSendCustomCharByIndex( uint8_t charIndex );

void lcdSendString( char* str );
void lcdSendStringClearLine( char* str );
void lcdSendStringFormXY( char* str, uint8_t x, uint8_t y );
void lcdSendStringFormXYClearLine( char* str, uint8_t x, uint8_t y );

void lcdSendInt( int64_t value );
void lcdSendIntClearLine( int64_t value );
void lcdSendIntFormXY( int64_t value, uint8_t x, uint8_t y );
void lcdSendIntFormXYClearLine( int64_t value, uint8_t x, uint8_t y );

void lcdSendFloat( float value, uint32_t decDigits );
void lcdSendFloatClearLine( float value, uint32_t decDigits );
void lcdSendFloatFormXY( float value, uint32_t decDigits, uint8_t x, uint8_t y );
void lcdSendFloatFormXYClearLine( float value, uint32_t decDigits, uint8_t x, uint8_t y );

#define lcdSendStringLn(str)   lcdSendString(str); \
                               lcdSendEnter()

/*==================[c++]====================================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* _LCD_H_ */

