/*==================[inclusions]=============================================*/

#include "sapi_datatypes.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

/*
 * Null Function Pointer definition
 * --------------------------------------
 * param:  void * - Not used
 * return: bool_t - Return always true
 */
bool_t sAPI_NullFuncPtr(void *ptr)
{
   return 1;
}

/*==================[ISR external functions definition]======================*/

/*
// FUNCTION POINTER VECTOR EXAMPLE
// Función para no tener NULL pointer
   void dummy(void){
   }
// Definición de un tipo con typedef.
   typedef void (*voidFunctionPointer_t)(void);
// Definición de una variable con el tipo de typedef, incializo en dummy (NULL)
   voidFunctionPointer_t voidFunctionPointer[2] = {dummy, dummy};
// Ejecuto la funcion
   (* voidFunctionPointer[0] )();
   (* voidFunctionPointer[1] )();
// Asigno una funcion a cada posición del vector
   voidFunctionPointer[0] = ledB;
   voidFunctionPointer[1] = led1;
*/


/** @} doxygen end group definition */
/*==================[end of file]============================================*/
