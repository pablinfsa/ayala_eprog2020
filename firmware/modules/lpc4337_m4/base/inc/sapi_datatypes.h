#ifndef _DATATYPES_H_
#define _DATATYPES_H_
/*==================[inclusions]=============================================*/
#include "stdint.h"
#include "chip.h"
//#include "board.h"
/*==================[c++]====================================================*/
#ifdef __cplusplus
extern "C" {
#endif
/*==================[macros]=================================================*/
/* Functional states */
#ifndef ON
#define ON     1
#endif
#ifndef OFF
#define OFF    0
#endif

/* Electrical states */
#ifndef HIGH
#define HIGH   1
#endif
#ifndef LOW
#define LOW    0
#endif

/* Logical states */

#ifndef FALSE
#define FALSE  0
#endif
#ifndef TRUE
#define TRUE   (!FALSE)
#endif

// From: https://es.coursera.org/lecture/embedded-software-hardware/9-register-definition-files-6pqVq

//  __I Defines 'read only' permissions: volatile const
//  __O Defines 'write only' permissions: volatile
// __IO Defines 'read / write' permissions: volatile

#define HW_REG_8_R(x)     (*((__I  uint8_t *)(x)))
#define HW_REG_16_R(x)    (*((__I uint16_t *)(x)))
#define HW_REG_32_R(x)    (*((__I uint32_t *)(x)))

#define HW_REG_8_W(x)     (*((__O  uint8_t *)(x)))
#define HW_REG_16_W(x)    (*((__O uint16_t *)(x)))
#define HW_REG_32_W(x)    (*((__O uint32_t *)(x)))

#define HW_REG_8_RW(x)    (*((__IO  uint8_t *)(x)))
#define HW_REG_16_RW(x)   (*((__IO uint16_t *)(x)))
#define HW_REG_32_RW(x)   (*((__IO uint32_t *)(x)))

// Example:
//#define REG_NAME   (HW_REG_32_RW(0x4544555))

/*==================[typedef]================================================*/

/* Define Boolean Data Type */
typedef uint8_t bool_t;

/* Define real Data Types (floating point) */
typedef float  real32_t;
//typedef double real64_t; // In LPC4337 float = double (Floating Point single precision, 32 bits)

/* Define Tick Data Type */
typedef uint64_t tick_t;

/*
 * Function Pointer definition
 * --------------------------------------
 * param:  void * - For passing arguments
 * return: bool_t - For Error Reports
 */
typedef bool_t (*sAPI_FuncPtr_t)(void *);

/*
 * Function Pointer definition
 * --------------------------------------
 * param:  void
 * return: void
 */
typedef void (*callBackFuncPtr_t)(void *);

/*==================[external functions declaration]=========================*/

/*
 * Null Function Pointer definition
 * --------------------------------------
 * param:  void * - Not used
 * return: bool_t - Return always true
 */
bool_t sAPI_NullFuncPtr(void *);

/*==================[c++]====================================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* _DATATYPES_H_ */
