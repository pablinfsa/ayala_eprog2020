#ifndef _SAPI_H_
#define _SAPI_H_
/*==================[inclusions]=============================================*/
#include "sapi_datatypes.h"
// Peripheral Drivers
//#include "board.h"                  // Use clock peripheral
#include "gpio.h"                   // Use GPIO peripherals
// High Level drivers

#include "stdio.h"                  // Use sapi_uart module

//#include "sapi_convert.h"                // Use <string.h>
#include "delay.h"                  // Use sapi_tick module
// External Peripheral Drivers
#include "lcd.h"                    // Use sapi_gpio peripherals
/*==================[c++]====================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/*==================[c++]====================================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* _SAPI_H_ */
