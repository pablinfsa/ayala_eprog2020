/*==================[inclusions]=============================================*/
#include "../inc/medidor.h"       /* <= own header */
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "chip.h"

/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/
int main(void)
{
	gpio_t echo=T_FIL2, trigger=T_FIL3;
	SystemClockInit();
	HcSr04Init(echo, trigger);
	LedsInit();
	int16_t distancia=0;


	while(1)
    {
		distancia=HcSr04ReadDistanceCentimeters(echo,trigger);
		if(distancia<10)
		{
			GPIOOn(LEDRGB_B);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOn(LED_3);
		};
		if(10<=distancia&&distancia<20)
		{
			GPIOOn(LEDRGB_B);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOff(LED_3);
		};
		if(20<=distancia&&distancia<30)
		{
			GPIOOn(LEDRGB_B);
			LedOn(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		};
		if(30<=distancia&&distancia<40)
		{
			GPIOOn(LEDRGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		};
		if(40<=distancia&&distancia<60)
		{
			GPIOOff(LEDRGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		};
		DelayUs(1);

    }
}

/*==================[end of file]============================================*/

