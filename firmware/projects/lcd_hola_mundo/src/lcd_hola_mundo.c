
/*==================[inclusions]=============================================*/
#include "../inc/lcd_hola_mundo.h"       /* <= own header */
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "lcd.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	SystemClockInit();
	DelayUs(LCD_STARTUP_WAIT_MS );  //tiempo de estabilización del LCD
	lcdInit(16, 2, 5, 8);//configura la correlación de puertos con pines
	LedsInit();
	lcdCursorSet( LCD_CURSOR_OFF ); //Apaga el cursor
	lcdClear();
	char str[]="Hola mundo";
	char *p_str;
	p_str=str;

	while(1)
    {
		lcdCursorSet( LCD_CURSOR_OFF );
		lcdGoToXY( 0, 0 );
		//DelaySec(1);
		//lcdClear(); // Borrar la pantalla
		lcdSendString(p_str);
		DelaySec(2);
		lcdClear(); // Borrar la pantalla

    }
	return 0;
}


/*==================[end of file]============================================*/

