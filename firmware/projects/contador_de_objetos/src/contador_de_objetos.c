/*==================[inclusions]=============================================*/
#include "../inc/contador_de_objetos.h"       /* <= own header */

#include "systemclock.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	gpio_t dout=T_COL0;
	SystemClockInit();
	LedsInit();
	Tcrt5000Init(dout);
	int16_t cantidad=0;

	/*start*/
	LedOn(LED_RGB_R);
	DelayUs(500);
	LedOff(LED_RGB_R);
	DelayUs(500);
	LedOn(LED_RGB_R);
	DelayUs(500);
	LedOff(LED_RGB_R);
	/**/

	while(1)
    {
			while(Tcrt5000State(dout))
			{
				DelayUs(100);
				if (!Tcrt5000State(dout))
				{
					cantidad++;
					LedOn(LED_RGB_R);
					DelayUs(300);
					LedOff(LED_RGB_R);
				}
			}
			if(cantidad>3){LedOn(LED_RGB_B);}
	}
}

/*==================[end of file]============================================*/

