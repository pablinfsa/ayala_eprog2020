/*==================[inclusions]=============================================*/
#include "../inc/ejercicio2.h"
#include "chip.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "delay.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
serial_config puerto_serie;
uint8_t channel=CH2;
uint32_t prom=0;
uint16_t valor=0;
uint32_t valores=0;
uint8_t i=0;
#define timer_lectura 100
#define timer_envio 1000
/*==================[internal functions declaration]=========================*/
void convertirAD(void);
void startAD(void);
void calcular_promedio(void);
/*==================[external data definition]===============================*/
timer_config timerAD = {TIMER_A,timer_lectura,&startAD};//toma un valor cada 100ms
timer_config timer_send = {TIMER_B,timer_envio,&calcular_promedio};//saca promedio por segundo

/*==================[external functions definition]==========================*/
void enviar()
{
	/*envio dato por usb*/
	UartSendString(SERIAL_PORT_PC,UartItoa(prom,10));
	UartSendString(SERIAL_PORT_PC,"\r");
}

void startAD(void)
{
	AnalogStartConvertion();

}

void convertirAD()
{
	if(i==9)
	valores=0;
	AnalogInputRead(channel, &valor);
	valores=valores+valor;//acumulo diez valores para el promedio
	i++;

}
void calcular_promedio()
{
	prom=valores/10;
	enviar();
}

/*==================[Función principal]==========================*/
int main(void)
{

	SystemClockInit();
	TimerInit(&timerAD);
	TimerStart(TIMER_A);
	TimerInit(&timer_send);
	TimerStart(TIMER_B);


	puerto_serie.port=SERIAL_PORT_PC;
	puerto_serie.baud_rate=115200;
	puerto_serie.pSerial=NULL;
	UartInit(&puerto_serie);


	analog_input_config entradaAD;
	entradaAD.input=CH1;
	entradaAD.mode=AINPUTS_SINGLE_READ;
	entradaAD.pAnalogInput=calcular_promedio;

	AnalogInputInit(&entradaAD);
	AnalogOutputInit();

	while(1)
    {

	}
}
/*==================[end of file]============================================*/
