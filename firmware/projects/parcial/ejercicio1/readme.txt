﻿Mide la altura de un liquido por medio de un sensor ultrasonido hc_sr4, 
calcula su volumen y la envía por UART 5 veces por segundos.
El sensor debe colocarse a 12cm de la altura de la base del recipiente.
Conexionado:
Ultrasonido hc_sr4       Edu-ciaa
GND----------------------GND
Vcc----------------------5V
echo---------------------T_FIL2
trigger------------------T_FIL3