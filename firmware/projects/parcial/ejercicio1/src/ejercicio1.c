/*==================[inclusions]=============================================*/
#include "../inc/ejercicio1.h"
#include "chip.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
bool medir=false;
bool hold=false;
gpio_t echo=T_FIL2, trigger=T_FIL3;
int16_t distancia=0;
serial_config puerto_serie;
uint16_t radio=3;
uint16_t volumen=3;
uint8_t base=10;
#define timer_calc 200
/*==================[internal functions declaration]=========================*/
void enviar_volumen(void);
void mostrar(void);
/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,timer_calc,&enviar_volumen};
/*==================[external functions definition]==========================*/

void SysInit()
{
	SystemClockInit();
	TimerInit(&my_timer);

	puerto_serie.port=SERIAL_PORT_PC;
	puerto_serie.baud_rate=115200;
	puerto_serie.pSerial=NULL;
	UartInit(&puerto_serie);
}
/*función que es llamada por leer distancia para enviar el dato*/
void mostrar()
{
	UartSendString(SERIAL_PORT_PC,UartItoa(volumen,10));
	UartSendString(SERIAL_PORT_PC," cm3\n\r");
}
/*calculo la distancia y envío cada por cada llamada del timer*/
void enviar_volumen(void)
{
	//calculo la altura del liquido con el sensor a 2cm de la altura máx del liquido por seguridad
	distancia=12-HcSr04ReadDistanceCentimeters(echo,trigger);
	volumen=3.14*distancia*radio*radio;
	mostrar();
}
/*==================[Función principal]==========================*/
int main(void)
{
	SysInit();
	TimerStart(TIMER_A);

	while(1)
    {

    }
}
/*==================[end of file]============================================*/
