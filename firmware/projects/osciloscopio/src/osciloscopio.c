/*==================[inclusions]=============================================*/
#include "../inc/osciloscopio.h"
#include "chip.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "delay.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};
serial_config puerto_bluetooth;
serial_config puerto_serie;
uint8_t channel=CH1;
uint16_t valor=0;
uint16_t valor_filt=0;
uint8_t i=0;
uint8_t fc=0;
uint16_t j=0;
float a=0;
//const char fc[6]={0,2,5,10,15,20};
float rc=0;
uint16_t aux=0;
bool filtrar=false;
uint8_t dat=0;
/*==================[internal functions declaration]=========================*/
void convertirAD(void);
void startAD(void);
void convertirDA(void);
float filtro(void);
void leer_caracter(void);
/*==================[external data definition]===============================*/
timer_config my_timerAD = {TIMER_A,2,&startAD};//convertir cada 2ms
timer_config my_timerDA = {TIMER_B,4,&convertirDA};//toma valor de la tabla

/*==================[external functions definition]==========================*/


void enviar()
{
	/*envio dato al osciloscopio bluetooth*/
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*G");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"X");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(j,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"Y");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(valor,10));

	UartSendString(SERIAL_PORT_P2_CONNECTOR,",X");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(j,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"Y");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(valor_filt,10));

	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");

	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*T");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(fc,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"Hz");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");


	/*envio dato al osciloscopio USB*/
	UartSendString(SERIAL_PORT_PC,UartItoa(valor,10));
	UartSendString(SERIAL_PORT_PC,"\r");

}

void startAD(void)
{
	AnalogStartConvertion();
}
void convertirAD()
{
	AnalogInputRead(channel, &valor);

	/*FILTRO*/
	if(filtrar==true)
	{
	valor_filt=aux+filtro()*(valor-aux);
	aux=valor_filt;
	}
	/*fin FILTRO*/

	enviar();
	valor_filt=valor;
	j++;
		if(j==1000)
	{
		j=0;
		UartSendString(SERIAL_PORT_P2_CONNECTOR,"*GC*");
	}
}
void convertirDA()
{
	AnalogOutputWrite(ecg[i]);
	i++;
	if(i==(BUFFER_SIZE-1)){i=0;}
}
float filtro()
{
	rc=1/(fc*3.1416*2);//0.0159
	a=0.002/(rc+0.002);
	return a;
}

void leer_caracter(void)
{
	UartReadByte(SERIAL_PORT_P2_CONNECTOR,&dat);
	if(dat=='F'){filtrar=true;}
	if(dat=='N'){filtrar=false;}
	if(dat=='U')
	{
		fc++;
		if(fc>20){fc=20;}
	}

	if(dat=='D')
	{
		fc--;
		if(fc<1){fc=1;}
	}
}
/*==================[Función principal]==========================*/
int main(void)
{

	SystemClockInit();
	LedsInit();
	TimerInit(&my_timerAD);
	TimerStart(TIMER_A);
	TimerInit(&my_timerDA);
	TimerStart(TIMER_B);

	puerto_bluetooth.port=SERIAL_PORT_P2_CONNECTOR;
	puerto_bluetooth.baud_rate=115200;
	puerto_bluetooth.pSerial=leer_caracter;
	UartInit(&puerto_bluetooth);

	puerto_serie.port=SERIAL_PORT_PC;
	puerto_serie.baud_rate=115200;
	puerto_serie.pSerial=NULL;
	UartInit(&puerto_serie);


	analog_input_config entradaAD;
	entradaAD.input=CH1;
	entradaAD.mode=AINPUTS_SINGLE_READ;
	entradaAD.pAnalogInput=convertirAD;

	AnalogInputInit(&entradaAD);
	AnalogOutputInit();

	while(1)
    {

	}
}
/*==================[end of file]============================================*/
