﻿Envía y recibe datos por uart a un monitor en una app (Bluetooth Electronics) que corre en 
SO Android.
La señal se toma de un vector, hace la conversión DA, la saca por analógico y vuelve a 
leer haciendo la conversión AD. La envía por RS232 a través de un dispositivo bluetooth 
en dos canales.
Un canal es la señal original y el otro canal es la señal aplicándole un filtro cuya 
frecuencia de corte se puede variar desde la aplicación enviándole caracteres específicos.

Link del video:
https://youtu.be/sWnVnRsZ6cA