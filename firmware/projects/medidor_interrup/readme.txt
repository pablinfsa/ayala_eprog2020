﻿Mide la distancia por medio de un sensor ultrasonido hc_sr4 y la muestra en un 
display LCD1602A utilizando interrupciones.
Envía y recibe datos a través de protocólo Rs232 con un módulo bluetooth HC-06 en comunicación 
con una aplicación que corre en android(app: bluetooth electronics)
También prende los led de la placa en función de la distancia y se activan las funciones a través
de teclas.

Link del video:
https://www.youtube.com/watch?v=A0WORsNfuwI