/*==================[inclusions]=============================================*/
#include "../inc/medidor_interrup.h"
#include "chip.h"
#include "systemclock.h"
#include "lcd.h"
#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
bool medir=false;
bool hold=false;
gpio_t echo=T_FIL2, trigger=T_FIL3;
int16_t distancia=0;
serial_config puerto_bluetooth;
uint32_t val=0;
uint8_t base=8;
uint8_t dat=0;

/*==================[internal functions declaration]=========================*/
void leer_distancia(void);
void leer_caracter(void);

void tecla1()
{
	medir=!medir;
	DelayMs(100);
}
void tecla2()
{
	hold=!hold;
	DelayMs(100);
}
/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,1000,&leer_distancia};
/*==================[external functions definition]==========================*/
void SysInit()
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);

	puerto_bluetooth.port=SERIAL_PORT_P2_CONNECTOR;
	puerto_bluetooth.baud_rate=9600;
	puerto_bluetooth.pSerial=leer_caracter;
	UartInit(&puerto_bluetooth);

	SwitchActivInt(SWITCH_1, tecla1);
	SwitchActivInt(SWITCH_2, tecla2);
	HcSr04Init(echo, trigger);
	DelayUs(LCD_STARTUP_WAIT_MS );  //tiempo de estabilización del LCD
	lcdInit(16, 2, 5, 8);//configura la correlación de puertos con pines
	lcdCursorSet( LCD_CURSOR_OFF ); //Apaga el cursor
	lcdClear();
}

void mostrar(int16_t distancia)
{
	if(hold==false)
	{
	lcdClear();
	lcdGoToXY( 0, 0 );
	lcdSendString("Distancia:");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*LR0G255B0*");
	}else
		{
			lcdClear();
			lcdGoToXY( 0, 0 );
			lcdSendString("Distancia(HOLD):");
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"*LR255G0B0*");
		}

	if(distancia<10)
	{
		GPIOOn(LEDRGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	};
	if(10<=distancia&&distancia<20)
	{
		GPIOOn(LEDRGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	};
	if(20<=distancia&&distancia<30)
	{
		GPIOOn(LEDRGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	};
	if(30<=distancia)
	{
		GPIOOn(LEDRGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
	};

	lcdGoToXY( 0, 1 );
	lcdSendString(UartItoa(distancia, 10));
	lcdGoToXY( 3, 1 );
	lcdSendString("cm");

	/*envío de distancia al monitor*/
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*MLa distancia es: *");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*M");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(distancia,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*Mcm*");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*M\r\n*");

	/*envío de distancia a la barra de medidas*/
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*G");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(distancia,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");

	DelayMs(600);
	lcdClear();
}

void leer_caracter(void)
{
	UartReadByte(SERIAL_PORT_P2_CONNECTOR,&dat);
	if(dat=='O'){medir=!medir;}
	if(dat=='H'){hold=!hold;}
}

void leer_distancia(void)
{
	if(hold==false)
	{
	distancia=HcSr04ReadDistanceCentimeters(echo,trigger);
	}
}
/*==================[Función principal]==========================*/
int main(void)
{
	SysInit();

	/*********mensaje inicial***********/
	lcdGoToXY( 0, 0 );
	lcdSendString("Bienvenido");
	DelaySec(5);
	lcdClear();
	/********************************/

	/*********calibración***********/
	LedOn(LED_3);
	lcdGoToXY( 0, 0 );
	lcdSendString("Ponga un objeto a 20cm");
	DelaySec(5);
	HcSr04Calib(echo,trigger);
	LedOff(LED_3);
	lcdClear();
	/********************************/

	TimerStart(TIMER_A);

	while(1)
    {
		if(medir==false)/*mensaje para iniciar mediciones, estado "stand by"*/
		{
			GPIOOff(LEDRGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			lcdGoToXY( 0, 0 );
			lcdSendString("Presione Tecla 1 para medir");
		}
			else/*estado "running"*/
			{
				mostrar(distancia);
			}
    }
}
/*==================[end of file]============================================*/
