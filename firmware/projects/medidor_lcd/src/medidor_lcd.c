/*==================[inclusions]=============================================*/
#include "../inc/medidor_lcd.h"
#include "systemclock.h"
#include "lcd.h"
#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "bool.h"


/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
uint8_t* Itoa(uint32_t val, uint8_t base)
{
	static char buf[32] = {0};

	uint32_t i = 30;

	for(; val && i ; --i, val /= base)

		buf[i] = "0123456789abcdef"[val % base];

	return &buf[i+1];
}
/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/
int main(void)
{
	gpio_t echo=T_FIL2, trigger=T_FIL3;
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	HcSr04Init(echo, trigger);

	int16_t distancia=0;
	bool medir=false;
	bool hold=false;
	//CalibrarCentimeters(echo,trigger);
	DelayUs(LCD_STARTUP_WAIT_MS );  //tiempo de estabilización del LCD
	lcdInit(16, 2, 5, 8);//configura la correlación de puertos con pines
	lcdCursorSet( LCD_CURSOR_OFF ); //Apaga el cursor
	lcdClear();

	lcdGoToXY( 0, 0 );
	lcdSendString("Bienvenido");
	DelaySec(5);
	lcdClear();

	LedOn(LED_3);
	lcdGoToXY( 0, 0 );
	lcdSendString("Ponga un objeto a 20cm");
	DelaySec(5);
	HcSr04Calib(echo,trigger);
	LedOff(LED_3);
	lcdClear();

	while(1)
    {

		while(medir==false)
		{
			GPIOOff(LEDRGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			lcdGoToXY( 0, 0 );
			lcdSendString("Presione Tecla 1 para medir");


			if(SwitchesRead()==SWITCH_1)
			{
				medir=!medir;
				DelayMs(200);
				lcdClear();
			}

		}
		while(medir==true)
		{
			if(SwitchesRead()==SWITCH_2)
			{
				hold=!hold;
				DelayMs(200);
			}

			if(hold==false)
			{
			lcdClear();
			lcdGoToXY( 0, 0 );
			lcdSendString("Distancia:");
			distancia=HcSr04ReadDistanceCentimeters(echo,trigger);

			}else
				{
					lcdClear();
					lcdGoToXY( 0, 0 );
					lcdSendString("Distancia(HOLD):");
				}

			if(distancia<10)
			{
				GPIOOn(LEDRGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			};
			if(10<=distancia&&distancia<20)
			{
				GPIOOn(LEDRGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			};
			if(20<=distancia&&distancia<30)
			{
				GPIOOn(LEDRGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOff(LED_3);
			};
			if(30<=distancia)
			{
				GPIOOn(LEDRGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			};

			lcdGoToXY( 0, 1 );
			lcdSendString(Itoa(distancia, 10));

			lcdGoToXY( 3, 1 );
			lcdSendString("cm");
			DelayMs(50);
			lcdClear();

			if(SwitchesRead()==SWITCH_1)
			{
				medir=!medir;
				DelayMs(100);
			}

		}
    }
}

/*==================[end of file]============================================*/
