/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../integrador_a/inc/main.h"

/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t n_led;       //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON 0, OFF 1, TOGGLE 2
}sled;

/*==================[internal functions declaration]=========================*/
void retardo(uint8_t periodo)
{
 sleep(periodo);
};
void prendeled(uint8_t led)
{
	printf("Se enciende el led %d \r\n",led);
};

void apagaled(uint8_t led)
{
	printf("Se apaga el led %d \r\n",led);

};

void toogle(uint8_t led,uint8_t ciclos,uint8_t periodo)
{

	uint8_t j=0;

	while(j<ciclos)
	{
		prendeled(led);
		retardo(periodo);
		apagaled(led);
		retardo(periodo);
		j++;
	}

}


void actualed(sled *led_ptr)
{

	switch(led_ptr->mode)
	{
		case(0):
			apagaled(led_ptr->n_led);
			break;
		case(1):
			prendeled(led_ptr->n_led);
			break;
		case(2):
			toogle(led_ptr->n_led,led_ptr->n_ciclos,led_ptr->periodo);
			break;
	}
}

int main(void)
{
	sled ledprueba, *led_ptr;
	led_ptr=&ledprueba;
//	ledprueba.mode;
//	ledprueba.n_led;
//	ledprueba.n_ciclos;
//	ledprueba.periodo;

	printf("Introduce un modo: \r\n");
	printf("Modo 0: apaga led \r\n");
	printf("Modo 1: enciende led \r\n");
	printf("Modo 2: modo toogle \r\n");
	scanf( "%" SCNu8,&ledprueba.mode);
	printf("Ingrese el número de led: \n");
	scanf( "%" SCNu8,&ledprueba.n_led);

	if(ledprueba.mode==2)
	{

	printf("Introduce número el ciclo: \n");
	scanf( "%" SCNu8,&ledprueba.n_ciclos);

	printf("Introduce el periodo: \n");
	scanf( "%" SCNu8,&ledprueba.periodo);
	}

	actualed(led_ptr);

	sleep(5);
	return(0);
}
/*==================[end of file]============================================*/

