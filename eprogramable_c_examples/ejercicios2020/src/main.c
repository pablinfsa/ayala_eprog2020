/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "time.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/


//////////////////////////ejercicio 1//////////////////////////////////

/*int main(void)
{
	int32_t var=0;
	int8_t mascara=(1<<6);
	var=var | mascara;
	printf("La variable es %d ",var);
	return 0;
}*/


/////////////////////////////////ejercicio 3////////////////////////////

/*int main(void)
{
	int16_t var=0xffff;
	int16_t mascara=(1<<14);
	var=var | ~mascara;
	printf("La variable es %d ",var);
	return 0;
}*/



///////////////////////////////ejercicio 7///////////////////////////////
/*
int main(void)
{
	uint32_t var=0xff00;

	int16_t mascara3=(1<<3);
	int16_t mascara13=~(1<<13);
	int16_t mascara14=~(1<<14);

	var=(var|mascara3);//sube el bit 3

	var=(var&mascara13);// baja el bit 13

	var=(var&mascara14);//baja el bit 14

	printf("máscara 3 %d \r\n",mascara3);

	printf("La variable modificada es %d ",var);
	return 0;
}
*/
////////////////////////////ejercicio 9///////////////////////////////////
//Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0.
//Si es 0, cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa.
/*
int main(void)
{
	int32_t var=0xFFFF; //1111 1111 1111 1111
	int32_t A;
	int16_t mascara=(1<<4); //sube el bit 4 0000 0000 0001 0000
    if ((var&mascara)==FALSE){A=0;} //0000 0000 0000 0000
    else {A=0xAA;} //1010 1010
    printf("La variable A es %x ",A);

    return 0;
}

*/
/////////////************ejercicio 12***/////////////////////////////

/*Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1. */
/*Luego, mediante máscaras, coloque un 0 en el bit 4.*/
/*
int main(void)
{
	int16_t *puntero;
	int16_t var=0xFFFA; //1111 1111 1111 1010
	int16_t aux=var;
    puntero=&var;
	int16_t mascara=~(1<<4);//Del ejercicio 7 "baja bit" 1111 1111 1110 1111

    *puntero=*puntero&mascara; // 1111 1111 1110 1010

    printf("variable antes de la máscara: %x \r\n" ,aux);

	printf("máscara: %x \r\npuntero: %x \r\nvariable: %x",mascara,*puntero,var);

    return 0;
}
*/

/////////////************ejercicio 14***/////////////////////////////

/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).
*/
/*
int main(void)
{

	typedef struct alumno{
			char nombre[12];
			char apellido[20];
			uint8_t edad;
			}alumno_t;

			alumno_t misdatos,*puntero;
			strcpy(misdatos.nombre,"pablo");


			puntero=&misdatos;

//	printf(" nombre:%s \r\n apellido:%s \r\n edad:%d \r\n ",*puntero.nombre,misdatos.apellido,misdatos.edad);


    return 0;
}
*/
/////////////************ejercicio 16***/////////////////////////////
/*
Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones
y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
Realice el mismo ejercicio, utilizando la definición de una “union”.
*/
/*
int main(void)
{
	uint8_t byte1=0;
	uint8_t byte2=0;
	uint8_t byte3=0;
	uint8_t byte4=0;
	uint32_t var=0;

	var=0x01020304;//00000001 00000010 00000011 00000100

	byte1=var;
	var=var<<8;
	byte2=var;
	var=var<<8;
	byte3=var;
	var=var<<8;
	byte4=var;
	var=var<<8;

	printf("variable: %x \r\nbyte1: %x \r\nbyte2: %x \r\nvbyte3: %x \r\nvbyte4: %x",var,byte1,byte2,byte3,byte4);

    return 0;
}
*/
////////////************ejercicio 16-b***/////////////////////////////
int main(void)
{
	typedef union
		{
			struct
			{
			uint8_t b1;
			uint8_t b2;
			uint8_t b3;
			uint8_t b4;

			}bytes;
			uint32_t var;
		}variable;

	variable nueva;
	nueva.var=0x01020304;
	nueva.bytes.b1=nueva.var;
	nueva.bytes.b2=nueva.var<<8;
	nueva.bytes.b3=nueva.var<<16;
	nueva.bytes.b4=nueva.var<<24;


	printf("variable: %" PRIu32 "\n",nueva.var);
	printf("byte 1: %" PRIu8 "\n",nueva.bytes.b1);
	printf("byte 2: %" PRIu8 "\n",nueva.bytes.b2);
	printf("byte 3: %" PRIu8 "\n",nueva.bytes.b3);
	printf("byte 4: %" PRIu8 "\n",nueva.bytes.b4);
    return 0;
}
/*==================[end of file]============================================*/


