/*==================[inclusions]=============================================*/
#include "../../integrador_c/inc/main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

void BinaryToBcd(uint32_t data,uint8_t digits,uint8_t *bcd_ptr)
{
	uint8_t i=0;
	for (i=0;i<digits;i++)
	{
		*bcd_ptr=data%10;//meto el resto en la dir apuntada por el puntero
		data=data/10;
		printf(" %d \r\n",*bcd_ptr);//muestra el resto por pantalla
		bcd_ptr++;
	}
}

int main(void)
{
   	uint8_t bcd[10];
	uint8_t *bcd_ptr;
	uint32_t data;
	uint8_t digits;
	bcd_ptr=bcd;

	printf("Introduce un número: ");
	scanf( "%" SCNu32,&data);//escaneo por consola el dato
	printf("Ingreso: %" PRIu32 "\n", data);
	printf("Introduce el número de dígitos: ");
	scanf( "%" SCNu8,&digits);//escaneo por consola el número de dígitos
	printf("Ingreso: %" PRIu8 "\n", digits);
	printf("los dígitos son: \n");

	BinaryToBcd(data,digits,bcd_ptr);
	sleep(10);


	return 0;
}

/*==================[end of file]============================================*/

