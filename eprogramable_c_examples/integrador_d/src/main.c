/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

/*==================[internal functions declaration]=========================*/

void controlpuertos(uint8_t bcd,gpioConf_t *sport_ptr)
{

	uint8_t i=0;
	for(i=0;i<4;i++)
	{
	printf("El puerto %d.%d ",sport_ptr->port,sport_ptr->pin);
	if(bcd%2==1){printf(" está en alto \r\n");}
		else{printf(" está en bajo \r\n");}
	bcd=bcd/2;
	sport_ptr++;
	}
}

int main(void)
{
	uint8_t bcd;
   	gpioConf_t puertos[4],*sport_ptr;
	puertos[0].port=1;
	puertos[1].port=1;
	puertos[2].port=1;
	puertos[3].port=2;
	puertos[0].pin=4;
	puertos[1].pin=5;
	puertos[2].pin=6;
	puertos[3].pin=14;
   	sport_ptr=puertos;

   	printf("Introduce un número de un dígito: ");
   	scanf( "%" SCNu8,&bcd);/*escaneo por consola el dato*/
   	printf("Ingreso: %" PRIu8 "\n",bcd);

   	controlpuertos(bcd,sport_ptr);

	sleep(10);

	return 0;
}

/*==================[end of file]============================================*/

